function searchParent(el, selector){
	var _el = el.parentElement;
	while(_el !== document && !_el.matches(selector)){
		_el = _el.parentElement;
	}

	return _el;
}

function toggleFunctionality(e){
	var el = searchParent(e.target, '.list-wrapper');

	document
	.querySelectorAll('.list-wrapper')
	.forEach(function(list){
		if(el !== list){
			list.classList.toggle('hidden-list');
		}else{
			list.classList.toggle('full-width-list');	
		}
	})
}

function inject(el){
	var toggle = document.createElement('span');
	toggle.className = 'toggle-full-width list-header-extras-menu dark-hover';
	toggle.addEventListener('click', toggleFunctionality);
	
	if(!el.classList.contains('js-add-list') && !el.querySelector('.toggle-full-width')){
		var header_buttons = el.querySelector('.list-header-extras');
		header_buttons.insertBefore(toggle, header_buttons.firstChild);
	}
}

function addLinks(card){

}

function addCustomTags(card){
	var card_title = card.querySelector('.list-card-title')
	if(card_title){
		var text = card_title.lastChild.textContent;

		var newText = text.replace(/#[\d\w\-]*/g, '')
		var matches = text.match(/#[\d\w\-]*/g) || []
		card_title.lastChild.textContent = newText;
		var drec_tags = card.querySelector('.drec-tags')
		if(drec_tags){
			drec_tags.parentElement.removeChild(drec_tags)
		}

		drec_tags = document.createElement('div')
		drec_tags.className = 'drec-tags'
		var badges = card.querySelector('.badges')
		badges.parentElement.insertBefore(drec_tags, badges)		

		matches.forEach(function(tag){
			tag = tag.substring(1).replace(/\-/g, ' ')

			var tag_element = document.createElement('span')
			tag_element.innerText = tag
			tag_element.className = 'drec-tag'
			drec_tags.append(tag_element)
		})
	}	
}

function displayTitle(card){
	addCustomTags(card);
	
}

function init(){
	document.querySelectorAll('.list-wrapper').forEach(function(el){
		inject(el);
	});
	
	document.querySelectorAll('.list-card').forEach(function(el){
		el.addEventListener('contextmenu', function(e){
			el.querySelector('.js-open-quick-card-editor.js-card-menu').click();
			e.preventDefault();
		})
		displayTitle(el)
	})

	
	// select the target node
	var target = document.querySelector('#board');
	
	// create an observer instance
	var observer = new MutationObserver(function(mutations) {
		mutations.forEach(function(mutation) {
			console.log(mutation)
			mutation.addedNodes.forEach(function(el){				
				if(el.nodeType === Node.ELEMENT_NODE && el.matches('.list-wrapper')){
					inject(el);
				}
				else if(el.nodeType === Node.ELEMENT_NODE && el.classList.contains('list-card')){
					//displayTitle(el)
					showId(el)
				}
				else if(el.nodeType === Node.ELEMENT_NODE && el.classList.contains('list-card-title')){
					var card = searchParent(el, '.list-card')
					displayTitle(card)
				}
				else if(el.nodeType === Node.TEXT_NODE && mutation.target.classList.contains('list-card-title')){
					var card = searchParent(mutation.target, '.list-card')
					displayTitle(card)
					setTimeout(function(){
						showId(card)
					}, 100)
				}
			});

		});
	});


	
	
	// configuration of the observer:
	var config = { childList: true, subtree: true};
	
	// pass in the target node, as well as the observer options
	observer.observe(target, config);

	//Show cards Ids
	setTimeout(function(){
		document.querySelectorAll('.list-card').forEach(showId)
	}, 100)
	
}

function showId(card){
	
	var el = card.querySelector('.card-short-id.hide')
	if(el){
		el.classList.remove('hide');
		el.innerText = '#' + el.innerText.substring(4)
	}
}


chrome.extension.sendMessage({}, function(response) {
	var readyStateCheckInterval = setInterval(function() {
		
	if (document.readyState === "complete") {
		
		clearInterval(readyStateCheckInterval);
		init();

		var observer = new MutationObserver(function(mutations) {
			mutations.forEach(function(mutation) {
				if(mutation.target.matches('#content')){
					init();
				}
			});
		});
		var config = { childList: true , subtree: false };
		observer.observe(document.querySelector('#content'), config);

	}
	}, 10);
});

